<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
	<title>GNOME-FR (Pays Francophones)</title>
</head>

<body>

<h1>Sortie de la version 2.12 de l'environnement de bureau et de la plate-forme de développement GNOME</h1>

<h3>La nouvelle version du populaire environnement de bureau offre encore plus de possibilités et est encore plus pratique à utiliser.</h3>

<p>BOSTON, Massachusetts – 7 septembre 2005 – Le projet GNOME livre aujourd'hui la dernière version de l'environnement de bureau et de la plate-forme de développement GNOME, environnement le plus avancé pour les systèmes d'exploitation Linux et Unix. La version 2.12 apporte des améliorations à GNOME en terme d'ergonomie en réponse aux retours des utilisateurs et grâce aux contributions des développeurs, et propose des milliers de changements qui affinent l'environnement de bureau libre le plus facile et le plus agréable à utiliser.</p>

<p>Tout en maintenant la philosophie du projet « les utilisateurs d'abord », GNOME sort de nouvelles versions stables tous les six mois. Cela permet aux développeurs et aux distributeurs de planifier en toute confiance leurs produits basés sur GNOME. Ainsi, on sait d'ores et déjà que les distributions telles que Fedora, SUSE et Ubuntu incluront GNOME 2.12 dans la prochaine version de leurs produits, fournissant ainsi GNOME 2.12 à des millions d'utilisateurs.</p>

<p>Parmi les fonctionnalités-clés de GNOME 2.12 :</p>
<ul>
  <li>un thème par défaut mis à jour ;</li>
  <li>une gestion des fichiers encore plus facile ;</li>
  <li>un comportement du presse-papier intuitif ;</li>
  <li>un visionneur de documents unifié ;</li>
  <li>le panneau de contrôle « À propos de moi ».</li>
</ul>

<p>Les administrateurs système découvriront avec plaisir le nouveau panneau de contrôle des services et un éditeur de menu. Les développeurs de logiciel bénéficent des améliorations de la couche sous-jacente GTK+, avec entre autres l'intégration de cairo, bibliothèque nouvelle génération d'affichage cairo de freedesktop.org.</p>

<p>« L'apparence de GNOME 2.12 est meilleure que tout ce que nous avons eu auparavant et apporte encore plus de consistence », indique Owen Taylor, président du conseil d'administration de la Fondation GNOME. « Il y a de nouvelles fonctionnalités très utiles dans tout l'environnement, implémentées avec soin pour pour améliorer l'ergonomie tout en gardant GNOME simple et intuitif. »</p>

<p>« Cette version est le résultat d'un travail colossal effectué par un extraordinaire groupe de personnes », ajoute Elijah Newren, responsable des sorties de version de GNOME. « Nous sommes fiers de nos développeurs et nous sommes fiers de ce que nous avons produit. »</p>

<p>GNOME 2.12 est disponible dans 43 langues, et de nombreuses autres langues sont partiellement supportées. Le galicien, l'estonien, l'indonésien, le macédonien, le népali, le slovaque, le vietnamien, le thaïlandais et le xhosa sont des langues nouvellement supportées dans GNOME 2.12.</p>

<h2>Disponibilité</h2>

<p>GNOME 2.12 sera bientôt disponible comme partie intégrante dans la plupart des distributions Linux populaires. Pour les plus impatients, le nouveau LiveCD GNOME (disponible à partir de <a href="http://www.gnome.org/start/2.12/" target="_top">www.gnome.org/start/2.12/</a>) permet aux utilisateurs et aux journalistes de tester la version la plus récente de l'environnement GNOME en gravant simplement un CD et en redémarrant l'ordinateur, sans aucune installation.</p>
<p>Les développeurs et les utilisateurs avancés qui souhaitent installer dès maintenant l'environnement de bureau et la plate-forme de développement GNOME 2.12 peuvent télécharger le logiciel à partir de <a href="http://ftp.gnome.org/pub/GNOME/desktop/" target="_top">http://ftp.gnome.org/pub/GNOME/desktop/</a> Le logiciel contient le code source de l'interface de bureau GNOME 2.12 et un ensemble complet d'outils et de bibliothèques de développement.</p>

<h2>À propos de GNOME</h2>

<p>GNOME est un projet de logiciel libre qui développe un environnement de bureau complet et simple à utiliser. En dehors des composants centraux de l'environnement de bureau comme les menus systèmes, le gestionnaire de fichiers, le navigateur web et des utilitaires, GNOME est aussi un environnement de développement complet permettant aux programmeurs de créer de nouvelles applications.</p>

<p>Plus de 500 développeurs de logiciels de tous les continents, dont plus de 100 développeurs payés, contribuent par leur temps et leurs efforts au projet. Des entreprises à la pointe du marché sponsorisent le projet, comme Fluendo, HP, IBM, Novell, Red Hat et Sun. GNOME fonctionne sous une grande variété de plate-formes, dont entre autres GNU/Linux (communément appelé Linux), l'environnement d'exploitation Solaris, HP-UX, Unix, BSD et Apple Darwin.</p>

<p>L'environnement de bureau GNOME est utilisé par des millions de personnes dans le monde. GNOME est une partie intégrante de toutes les distributions Linux et Unix les plus avancées dans le monde, dont notamment les distributions communautaires populaires comme Debian, Fedora Core et SUSE. GNOME est aussi l'environnement de bureau par défaut d'importantes distributions Linux pour entreprises telles que Red Hat Enterprise Linux et Sun Java Desktop System. GNOME est aussi l'environnement de bureau choisi pour certains des déploiements de bureaux Linux les plus importants dans le monde, comme les grands déploiements gouvernementaux en Estrémadure (Espagne) et à Sao Paulo (Brésil). La technologie GNOME est utilisée par d'importants fournisseurs de logiciels Linux comme Firefox, le projet Eclipse, Real Networks et VMWare.</p>

<p>Plus d'informations sur GNOME sont disponibles sur <a href="http://www.gnome.org">http://www.gnome.org/</a></p>

<h2>À propos de la Fondation GNOME</h2>

<p>Regroupant plusieurs centaines de développeurs bénévoles et des sociétés innovantes, la Fondation GNOME est une organisation destinée à supporter l'avancement de GNOME. La Fondation est une association à but non lucratif gérée par ses membres qui fournit la logistique ainsi que le support financier et légal pour le projet GNOME et aide à la détermination de sa vision et son plan de développement. Plus d'informations concernant la Fondation GNOME sont disponibles sur <a href="http://foundation.gnome.org/">foundation.gnome.org</a>.</p>

<h2>Contacts pour la presse</h2>
<p>Merci d'envoyer toutes les demandes concernant la presse à <a href="mailto:contact-presse@gnomefr.org">contact-presse@gnomefr.org</a>.</p>

<p><em>Linux est une marque déposée de Linus Torvalds. Tous les autres noms et marques déposées sont les propriétés de leurs détenteurs respectifs.</em></p>

</body>

</html>
