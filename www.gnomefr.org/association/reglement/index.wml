<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
	<title>GNOME-FR (Pays Francophones)</title>
</head>

<body>

<h1>Règlement intérieur de l'association GNOME-FR</h1>

<p>Le réglement intérieur est aussi disponible au format <a href="reglement_interieur.pdf">PDF</a>.</p>

    <div class="TOC">
      <dl>
        <dt><b>Table des matières</b></dt>

        <dt>1. <a href="#AEN3">Article 1 - Réalisation des objectifs</a></dt>

        <dt>2. <a href="#AEN16">Article 2 - Cotisations</a></dt>

        <dd>
          <dl>
            <dt>2.1. <a href="#AEN18">Membres actifs et membres adhérents</a></dt>

            <dt>2.2. <a href="#AEN21">Membres bienfaiteurs</a></dt>
          </dl>
        </dd>

        <dt>3. <a href="#AEN24">Article 3 - Définition de l'année en cours</a></dt>

        <dt>4. <a href="#AEN27">Article 4 - Procédure en cas de non-paiement d'une cotisation</a></dt>

        <dt>5. <a href="#AEN30">Article 5 - Conditions d'admission des membres</a></dt>

        <dt>6. <a href="#AEN33">Article 6 - Courrier électronique authentifié</a></dt>

        <dt>7. <a href="#AEN36">Article 7 - Assemblées générales et réunions à distance</a></dt>

        <dt>8. <a href="#AEN41">Article 8 - Modalités d'élection</a></dt>

        <dt>9. <a href="#AEN46">Article 9 - Déclaration de candidature</a></dt>

        <dt>10. <a href="#AEN49">Article 10 - Vote par correspondance</a></dt>

        <dt>11. <a href="#AEN52">Article 11 - Acquisitions, aliénations ou locations immobilières</a></dt>

        <dt>12. <a href="#AEN55">Article 12 - Remboursement des dépenses</a></dt>

        <dt>13. <a href="#AEN58">Article 13 - Commissaires aux comptes</a></dt>

        <dt>14. <a href="#AEN61">Article 14 - Utilisation du logo de l'association</a></dt>
      </dl>
    </div>

    <div class="sect1">
      <h2 class="sect1"><a name="AEN3" id="AEN3">Article 1 - Réalisation des objectifs</a></h2>

      <p>L'association GNOME-FR, outre les objectifs définis à l'article 3.a des statuts, entend agir dans les domaines suivants :</p>

      <ul>
        <li>
          <p>mise en commun des connaissances et compétences et assistance aux membres de l'association dans le domaine du développement d'application avec la plate-forme GNOME,</p>
        </li>

        <li>
          <p>regroupement régulier des membres de l'association, dans un but d'échange et de cohésion (les membres ou correspondants éloignés de l'association pouvant être dispensés de présence régulière),</p>
        </li>

        <li>
          <p>participation à des manifestations telles que des conférences, des salons, des journées des associations et tout autre événement où la promotion des systèmes et logiciels libres est possible,</p>
        </li>

        <li>
          <p>participation à des projets de plus grande envergure, dans le cadre des objectifs de l'association, et avec des organisations extérieures aux objectifs similaires ou compatibles.</p>
        </li>
      </ul>

      <p>Cette liste n'est évidemment pas limitative, et toute action conforme aux statuts et non listée ci-dessus pourra cependant être entreprise après délibération et accord du conseil d'administration.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN16" id="AEN16">Article 2 - Cotisations</a></h2>

      <div class="sect2">
        <h3 class="sect2"><a name="AEN18" id="AEN18">2.1. Membres actifs et membres adhérents</a></h3>

        <p>Ils sont exempts de cotisation.</p>
      </div>

      <div class="sect2">

        <h3 class="sect2"><a name="AEN21" id="AEN21">2.2. Membres bienfaiteurs</a></h3>

        <p>Leur cotisation est fixée à un minimum de 100 euros pour l'année de l'inscription (voir article 3).</p>
      </div>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN24" id="AEN24">Article 3 - Définition de l'année en cours</a></h2>

      <p>L'année de fonctionnement de l'association et donc de cotisation est fixée du 1er septembre au 31 août de l'année civile suivante.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN27" id="AEN27">Article 4 - Procédure en cas de non-paiement d'une cotisation</a></h2>

      <p>En cas de non-paiement d'une cotisation par un membre, le Trésorier doit contacter le membre par courrier postal ou courrier électronique authentifié pour l'en avertir et l'inviter à fournir des explications. Si le membre ne paie pas sa cotisation dans un délai d'un mois après l'avertissement, le Conseil d'Administration peut exclure l'intéressé pour non-paiement de cotisation.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN30" id="AEN30">Article 5 - Conditions d'admission des membres</a></h2>

      <p>L'adhésion de nouveaux membres actifs se fait par la co-optation par deux membres actifs, avec possibilité de veto par un autre membre actif.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN33" id="AEN33">Article 6 - Courrier électronique authentifié</a></h2>

      <p>La définition d'un courrier électronique authentifié est subordonnée aux possibilités de chiffrement et d'authentification disponibles. Dans l'état actuel des choses, aucun courrier électronique ne peut être considéré comme authentifiable.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN36" id="AEN36">Article 7 - Assemblées générales et réunions à distance</a></h2>

      <p>L'association GNOME-FR se réserve le droit de considérer comme valable la participation à une réunion du conseil d'administration ou du bureau d'un membre empêché dès lors que celui-ci a fait parvenir à au moins un des membres du bureau un courrier précisant sa position sur au moins la moitié des points portés à l'ordre du jour.</p>

      <p>L'association GNOME-FR se réserve en outre le droit de considérer que l'usage d'un forum privé situé sur son site internet, d'un canal IRC privé et réservé à ses membres puisse être assimilé à une réunion ayant même valeur qu'une assemblée générale, sous réserve que l'ordre du jour de cette réunion virtuelle ait été clairement annoncée sur le site ou sur le channel IRC réservé aux membres, et que les conditions de quorum d'une assemblée générale soient respectées.</p>

      <p>Le rapport moral, le rapport financier et le budget prévisionnel, ainsi que toute information se rapportant à l'ordre du jour, pourront être consultés sur le site réservé aux membres quinze jours avant l'assemblée générale.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN41" id="AEN41">Article 8 - Modalités d'élection</a></h2>

      <p>Le droit de vote est réservé aux membres actifs et bienfaiteurs, à jour de cotisation, et faisant partie de l'association depuis au moins trois mois jour pour jour à la date effective du vote. Est éligible tout membre actif satisfaisant aux mêmes conditions.</p>

      <p>Le conseil d'administration se réserve cependant le droit de réduire cette durée à titre exceptionnel. Cette décision devra être dûment motivée lors de l'assemblée générale.</p>

      <p>Aucune condition de nationalité n'est requise, ni pour l'adhésion, ni pour l'élection au conseil d'administration.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN46" id="AEN46">Article 9 - Déclaration de candidature</a></h2>

      <p>Les candidatures au conseil d'administration doivent être adressées au bureau par courrier postal ou courrier électronique authentifié au moins sept jours avant la date de l'assemblée générale. Celles-ci devront être accompagnées d'une présentation du candidat et de sa profession de foi.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN49" id="AEN49">Article 10 - Vote par correspondance</a></h2>

      <p>Lorsque le sujet d'un vote est connu et fixé à l'avance, un vote physique peut être complété par un vote électronique réalisé préalablement (et donc clos préalablement pour permettre un décompte des bulletins électroniques). Les personnes ayant choisi le vote électronique ne peuvent participer au vote physique (en étant présentes ou représentées).</p>

      <p>Le résultat du vote est proclamé en prenant en compte les bulletins électroniques et le vote des personnes présentes et représentées. Le nombre de votants pour les questions éventuelles de quorum est donc la somme des nombres de bulletins électroniques et physiques. Toute contestation soulevée sur la validité d'un vote électronique doit être examinée et tranchée par le conseil d'administration, avec consignation dans le compte-rendu.</p>

      <p>Le conseil d'administration déterminera en fonction des techniques et de la règlementation du moment les procédures de vote électronique.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN52" id="AEN52">Article 11 - Acquisitions, aliénations ou locations immobilières</a></h2>

      <p>Pour l'instant le règlement intérieur ne prévoit pas de règles plus contraignantes que celles des statuts.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN55" id="AEN55">Article 12 - Remboursement des dépenses</a></h2>

      <p>En ce qui concerne les remboursements de frais de déplacement, ceux-ci auront lieu sur présentation du titre de transport ou de la facture des frais engagés (carburant et péages). Les membres sollicitant le remboursement de leurs frais de carburant s'engagent à faire en sorte que cette facture soit représentative de leurs dépenses effectives, par exemple en faisant le plein au début et à la fin du voyage et en fournissant la seconde facture.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN58" id="AEN58">Article 13 - Commissaires aux comptes</a></h2>

      <p>L'assemblée générale peut décider de désigner un ou plusieurs commissaires aux comptes si le budget prévisionnel présenté par le Trésorier le nécessite. Ces commissaires aux comptes seront élus parmi les membres éligibles de l'association.</p>
    </div>

    <div class="sect1">

      <h2 class="sect1"><a name="AEN61" id="AEN61">Article 14 - Utilisation du logo de l'association</a></h2>

      <p>Les membres actifs peuvent faire référence à leur affiliation à l'association, à condition d'en respecter les buts et la déontologie.</p>

      <p>L'utilisation du ou des logos de l'association sur un document papier est soumise expressément à l'accord du Président. Sur un document hypermédia qui respecte l'esprit et la lettre des statuts de l'association, elle est subordonnée à l'existence d'un lien hypertexte du logo vers le site officiel de l'association, ou vers un miroir de ce site agréé par l'association.</p>
    </div>

</body>

</html>
